﻿using System;
using System.ComponentModel;

namespace LZFaCompressor
{
    public class LZFaCompletedEventArgs : AsyncCompletedEventArgs
    {
        private LZFaTask _task;

        internal LZFaCompletedEventArgs(Exception e, bool canceled, object userState) : base(e, canceled, userState)
        {
            LZFaTask task = userState as LZFaTask;
            if (task != null)
            {
                _task = task;
            }
        }

        public LZFaTask Task
        {
            get
            {
                RaiseExceptionIfNecessary();
                return _task;
            }
        }
    }
}
