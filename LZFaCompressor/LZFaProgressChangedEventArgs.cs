﻿using System.ComponentModel;

namespace LZFaCompressor
{
    public class LZFaProgressChangedEventArgs : ProgressChangedEventArgs
    {
        LZFaTaskState _state;

        internal LZFaProgressChangedEventArgs(int progressPercentage, object userState) : base(progressPercentage, userState)
        {
            LZFaTask _task = userState as LZFaTask;
            if (_task != null)
            {
                _state = _task.TaskState;
            }
        }

        public LZFaTaskState TaskState
        {
            get { return _state; }
        }
    }
}