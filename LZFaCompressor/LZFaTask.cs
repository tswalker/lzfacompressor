﻿using System;

namespace LZFaCompressor
{
    /// <summary>
    /// A general container to pass values to and from the asynchronous worker threads.
    /// </summary>
    public class LZFaTask
    {
        private string _taskID;
        private LZFaTaskType _taskType;
        private LZFaTaskState _taskState;
        private int _bufferSize = 0;
        private int _shimSize = 0;
        private int _decompressedSize = 0;
        private int _compressedSize = 0;
        private string _sourceFile = string.Empty;
        private string _targetFile = string.Empty;

        /// <summary>
        /// A general container to pass values to and from the asynchronous worker threads.
        /// </summary>
        public LZFaTask()
        {
            _taskID = Guid.NewGuid().ToString();
        }

        /// <summary>
        /// A general container to pass values to the asynchronous worker threads.
        /// </summary>
        /// <param name="taskType">LZFaTaskType (optional)<para>Default is set to LZF_COMPRESSION</para></param>
        public LZFaTask(LZFaTaskType taskType)
        {
            _taskID = Guid.NewGuid().ToString();
            _taskType = taskType;
        }

        /// <summary>
        /// A unique identifier for the task.
        /// </summary>
        public string TaskID
        {
            get { return _taskID; }
        }

        public LZFaTaskType TaskType
        {
            get { return _taskType; }
        }

        /// <summary>
        /// Task State
        /// <para>
        /// The TaskState changes while an asynchronous operation works on the task provided.
        /// </para>
        /// </summary>
        public LZFaTaskState TaskState
        {
            get { return _taskState; }
            set { _taskState = value; }
        }

        /// <summary>
        /// The initial size of the buffer to use when compressing or decompressing data.
        /// </summary>
        public int BufferSize
        {
            get { return _bufferSize; }
            set { _bufferSize = value; }
        }

        /// <summary>
        /// The shim size is a value by which the internal buffers grow.
        /// </summary>
        public int ShimSize
        {
            get { return _shimSize; }
            set { _shimSize = value; }
        }

        /// <summary>
        /// The size of the data in bytes of the decompressed file.
        /// </summary>
        /// <para>
        /// The value returned is typeof(int)
        /// </para>
        public int DecompressedSize
        {
            get { return _decompressedSize; }
            internal set { _decompressedSize = value; }
        }

        /// <summary>
        /// The size of the data in bytes of the compressed file.
        /// <para>
        /// The value returned is typeof(int)
        /// </para>
        /// </summary>
        public int CompressedSize
        {
            get { return _compressedSize; }
            internal set { _compressedSize = value; }
        }

        /// <summary>
        /// File to read data from when performing operation
        /// </summary>
        public string SourceFile
        {
            get { return _sourceFile; }
            set { _sourceFile = value; }
        }

        /// <summary>
        /// File to save data to when performing the operation
        /// </summary>
        public string TargetFile
        {
            get { return _targetFile; }
            set { _targetFile = value; }
        }
    }

    /// <summary>
    /// The type of operation to be performed on the task
    /// </summary>
    public enum LZFaTaskType
    {
        LZF_COMPRESSION,
        LZF_DECOMPRESSION
    }

    /// <summary>
    /// The task state defines the level of processing an operation has performed on a task.
    /// </summary>
    public enum LZFaTaskState
    {
        INITIALIZED,
        PENDING,
        PROCESSING,
        COMPLETED,
        CANCELED,
        ERROR
    }
}
