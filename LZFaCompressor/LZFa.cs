﻿/*
 * Asynchronous version Port: (File Compression & Decompression ONLY)
 * Unity Contributors:
 *  tswalk: https://forum.unity3d.com/members/tswalk.362341/
 * Date: 1/2017
 * Note: 
 *  1). Changed hslot to uint in compressor instead of long as all apparent operations
 *      resulted in uint values (never a negative).  Saves 4 bytes of value per HSIZE
 *      in the dictionary.
 *      (reference: code lzfP.h, line#:160  'typedef unsigned int LZF_HSLOT')
 *  2). Dictionary is now used instead of just HashTable as retrieving TValue based
 *      on given TKey is apparently quick.  This also provided a way to implement a
 *      choice for 'thread-safe' operations by locking the state while modifying
 *      the dictionary.
 *     
 * Alternate version(s) to C# Port:
 * Unity Contributors: 
 *  Agent_007: https://forum.unity3d.com/members/agent_007.78088/
 *  vexe:https://forum.unity3d.com/members/vexe.280515/
 *  
 * URL: https://forum.unity3d.com/threads/lzf-compression-and-decompression-for-unity.152579/#post-2903705
 * Date: ~2012-2014
 * 
 * Improved version to C# LibLZF Port:
 * Copyright (c) 2010 Roman Atachiants <kelindar@gmail.com>
 *
 * Original CLZF Port:
 * Copyright (c) 2005 Oren J. Maurice <oymaurice@hazorea.org.il>
 *
 * Original LibLZF Library  Algorithm:
 * Copyright (c) 2000-2008 Marc Alexander Lehmann <schmorp@schmorp.de>
 *
 * Redistribution and use in source and binary forms, with or without modifica-
 * tion, are permitted provided that the following conditions are met:
 *
 *   1.  Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2.  Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *   3.  The name of the author may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MER-
 * CHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPE-
 * CIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTH-
 * ERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * the GNU General Public License version 2 (the "GPL"), in which case the
 * provisions of the GPL are applicable instead of the above. If you wish to
 * allow the use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * BSD license, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL. If
 * you do not delete the provisions above, a recipient may use your version
 * of this file under either the BSD or the GPL.
*/
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Threading;

namespace LZFaCompressor
{
    public delegate void LZFaCompletedEventHandler(object sender, LZFaCompletedEventArgs e);
    public delegate void ProgressChangedEventHandler(object sender, LZFaProgressChangedEventArgs e);

    /// <summary>
    /// Provide asynchronous methods for file LZF compression and decompression
    /// </summary>
    /// <remarks>https://msdn.microsoft.com/en-us/library/ms228969(v=vs.110).aspx</remarks>
    /// 
    public class LZFa : Component
    {
        public event LZFaCompletedEventHandler LZFaCompleted;
        public event ProgressChangedEventHandler ProgressChanged;

        private delegate void LZFaCompressWorkerEventHandler(AsyncOperation asyncOp);
        private delegate void LZFaDecompressWorkerEventHandler(AsyncOperation asyncOp);

        private HybridDictionary userStateToLifetime = new HybridDictionary();
        private SendOrPostCallback onCompletedDelegate;
        private SendOrPostCallback onProgressReportDelegate;

        /*
         * NOTE:
         * small blocks: HLOG = 14 or 15 (14 recommended)
         * low-memory/faster config: HLOG = 13
         * best compression: 15 or 16 (up to 22)
         * HSIZE = (1 << HLOG);
         */
        private static readonly uint HLOG = 14; 
        private static readonly uint HSIZE = (1 << 14); //16K
        private static readonly uint MAX_LIT = (1 << 5);
        private static readonly uint MAX_OFF = (1 << 13);
        private static readonly uint MAX_REF = ((1 << 8) + (1 << 3));
        private Dictionary<string, uint[]> LZFTable = new Dictionary<string, uint[]>();

        //hook callbacks
        public LZFa()
        {
            onCompletedDelegate = new SendOrPostCallback(LZFaOperationCompleted);
            onProgressReportDelegate = new SendOrPostCallback(ReportProgress);
        }

        private void LZFaOperationCompleted(object operationState)
        {
            LZFaCompletedEventArgs e = operationState as LZFaCompletedEventArgs;
            OnLZFaOperationCompleted(e);
        }

        private void ReportProgress(object arg)
        {
            OnProgressChanged((LZFaProgressChangedEventArgs)arg);
        }

        protected void OnLZFaOperationCompleted(LZFaCompletedEventArgs e)
        {
            if (LZFaCompleted != null)
            {
                LZFaCompleted(this, e);
            }
        }

        private void OnProgressChanged(LZFaProgressChangedEventArgs e)
        {
            if (ProgressChanged != null)
            {
                ProgressChanged(this, e);
            }
        }

        private void CompletionMethod(Exception exception, bool canceled, AsyncOperation asyncOp)
        {
            if (!canceled)
            {
                lock (userStateToLifetime.SyncRoot)
                {
                    userStateToLifetime.Remove(asyncOp.UserSuppliedState);
                }
            }
            LZFaCompletedEventArgs e = new LZFaCompletedEventArgs(exception, canceled, asyncOp.UserSuppliedState);
            asyncOp.PostOperationCompleted(onCompletedDelegate, e); //asyncOp is no longer usable after this
        }

        private bool TaskCanceled(object task)
        {
            return (userStateToLifetime[task] == null);
        }

        /// <summary>
        /// Compress data based on task details passed through the asyncOp parameter. Executes on a worker thread.
        /// </summary>
        /// <param name="asyncOp">LZFaTask (required)</param>
        private void CompressFileWorker(AsyncOperation asyncOp)
        {
            Exception e = null;
            IAsyncResult streamState = null;
            LZFaTask task = asyncOp.UserSuppliedState as LZFaTask;
            task.TaskState = LZFaTaskState.PROCESSING;
            asyncOp.Post(onProgressReportDelegate, new LZFaProgressChangedEventArgs(0, asyncOp.UserSuppliedState));

            if (!TaskCanceled(asyncOp.UserSuppliedState))
            {
                try
                {
                    if (task.ShimSize <= 0) { throw new InvalidOperationException("bufferShim must be greater than 0."); }
                    byte[] input = File.ReadAllBytes(task.SourceFile);
                    task.DecompressedSize = input.Length;
                    int bufferSize = input.Length < task.ShimSize ? (input.Length + task.ShimSize) : input.Length; //is source really small?
                    byte[] buffer = new byte[bufferSize];
                    int byteCount = 0;

                    while (byteCount == 0 && !TaskCanceled(asyncOp.UserSuppliedState))
                    {
                        byteCount = LZFCompress(input, ref buffer, asyncOp);
                        if (byteCount == 0)
                        {
                            buffer = null; //probably not needed, but gets the point across
                            checked { bufferSize += task.ShimSize; } //throw overflow
                            buffer = new byte[bufferSize];  //would throw here on overflow anyway
                        }
                    }

                    //if not canceled, save file
                    if (!TaskCanceled(asyncOp.UserSuppliedState) && buffer.Length > 0 && byteCount > 0)
                    {
                        ManualResetEvent streamEvent = new ManualResetEvent(false);
                        FileStream stream = new FileStream(task.TargetFile, FileMode.Create, FileAccess.Write);
                        streamState = stream.BeginWrite(buffer, 0, byteCount, new AsyncCallback(StreamWriterCallback), new IOState(stream, streamEvent));

                        //wait for StreamWriterCallback to actually complete or task cancelation before unblocking
                        streamState.AsyncWaitHandle.WaitOne(0, streamEvent.WaitOne() || TaskCanceled(asyncOp.UserSuppliedState));
                        task.CompressedSize = byteCount;
                    }
                    buffer = null;
                }
                catch (Exception ex)
                {
                    e = ex;
                    task.TaskState = LZFaTaskState.ERROR;
                }
            }
            LZFTableRemoveKey(task.TaskID);
            bool streamCompleted = streamState == null ? false : streamState.IsCompleted; //incase task canceled before stream created
            task.TaskState = task.TaskState == LZFaTaskState.ERROR ? LZFaTaskState.ERROR : streamCompleted ? LZFaTaskState.COMPLETED : LZFaTaskState.CANCELED;
            asyncOp.Post(onProgressReportDelegate, new LZFaProgressChangedEventArgs(0, asyncOp.UserSuppliedState));
            CompletionMethod(e, TaskCanceled(asyncOp.UserSuppliedState), asyncOp);
        }

        /// <summary>
        /// Decompress data based on task details passed through the asyncOp parameter. Executes on a worker thread.
        /// </summary>
        /// <param name="asyncOp">LZFaTask (required)</param>
        private void DecompressFileWorker(AsyncOperation asyncOp)
        {
            Exception e = null;
            IAsyncResult streamState = null;
            LZFaTask task = asyncOp.UserSuppliedState as LZFaTask;
            task.TaskState = LZFaTaskState.PROCESSING;
            asyncOp.Post(onProgressReportDelegate, new LZFaProgressChangedEventArgs(0, asyncOp.UserSuppliedState));

            if (!TaskCanceled(asyncOp.UserSuppliedState))
            {
                try
                {
                    if (task.ShimSize <= 0) { throw new InvalidOperationException("ShimSize must be greater than 0."); }
                    byte[] input = File.ReadAllBytes(task.SourceFile);
                    task.CompressedSize = input.Length;
                    int bufferSize;

                    //start with a shim (may want to start with a shim >= 8K), ideally a shim isn't necessary :)
                    checked { bufferSize = task.BufferSize + task.ShimSize; }
                    byte[] buffer = new byte[bufferSize];
                    int byteCount = 0;
                    //too many iterations here results in very poor performance, make sure buffer is close or 'matches' known uncompressed file size
                    while (byteCount == 0 && !TaskCanceled(asyncOp.UserSuppliedState))
                    {
                        byteCount = LZFDecompress(input, ref buffer, asyncOp);
                        if (byteCount == 0)
                        {
                            buffer = null;
                            checked { bufferSize += task.ShimSize; }
                            buffer = new byte[bufferSize];
                        }
                    }

                    //if not canceled, save file
                    if (!TaskCanceled(asyncOp.UserSuppliedState) && buffer.Length > 0 && byteCount > 0)
                    {
                        ManualResetEvent streamEvent = new ManualResetEvent(false);
                        FileStream stream = new FileStream(task.TargetFile, FileMode.Create, FileAccess.Write);
                        streamState = stream.BeginWrite(buffer, 0, byteCount, new AsyncCallback(StreamWriterCallback), new IOState(stream, streamEvent));

                        //wait for StreamWriterCallback to actually complete or task cancellation before unblocking
                        streamState.AsyncWaitHandle.WaitOne(0, streamEvent.WaitOne() || TaskCanceled(asyncOp.UserSuppliedState));
                        task.DecompressedSize = byteCount;
                    }
                    buffer = null;
                }
                catch (Exception ex)
                {
                    e = ex;
                    task.TaskState = LZFaTaskState.ERROR;
                }
            }
            bool streamCompleted = streamState == null ? false : streamState.IsCompleted; //incase task canceled before stream created
            task.TaskState = task.TaskState == LZFaTaskState.ERROR ? LZFaTaskState.ERROR : (streamCompleted ? LZFaTaskState.COMPLETED : LZFaTaskState.CANCELED);
            asyncOp.Post(onProgressReportDelegate, new LZFaProgressChangedEventArgs(0, asyncOp.UserSuppliedState));
            CompletionMethod(e, TaskCanceled(asyncOp.UserSuppliedState), asyncOp);
        }

        /// <summary>
        /// Finalizes the file and sets a signal to ManualResetEvent provided by stateObject from BeginWrite.
        /// </summary>
        /// <param name="ar">IOState</param>
        private void StreamWriterCallback(IAsyncResult ar)
        {
            IOState _state = (IOState)ar.AsyncState;
            FileStream _stream = _state.IOFileStream;
            _stream.EndWrite(ar);
            _stream.Close();
            _state.IOStateEvent.Set(); //send signal
        }

        /// <summary>
        /// A class to provide stream and event handling through a callback function
        /// </summary>
        private class IOState
        {
            FileStream _stream;
            ManualResetEvent _event;

            /// <summary>
            /// A class to provide stream and event handling through a callback function
            /// </summary>
            /// <param name="stream">FileStream</param>
            /// <param name="e">ManualResetEvent</param>
            public IOState(FileStream stream, ManualResetEvent e)
            {
                _stream = stream;
                _event = e;
            }
            public FileStream IOFileStream
            {
                get { return _stream; }
            }
            public ManualResetEvent IOStateEvent
            {
                get { return _event; }
            }
        }

        /// <summary>
        /// Asynchronous LZF File Compressor
        /// </summary>
        /// <param name="task">LZFaTask</param>
        public virtual void CompressFileAsync(object task)
        {
            AsyncOperation asyncOp = AsyncOperationManager.CreateOperation(task);
            lock (userStateToLifetime.SyncRoot)
            {
                if (userStateToLifetime.Contains(task))
                {
                    throw new ArgumentException("Task ID parameter must be unique", "task");
                }
                LZFaTask _task = task as LZFaTask;
                if (_task == null) { throw new InvalidCastException("Task must be typeof(LZFaTask)"); }
                userStateToLifetime[task] = asyncOp;
                LZFTableAddKey(_task.TaskID);
            }
            LZFaCompressWorkerEventHandler workerDelegate = new LZFaCompressWorkerEventHandler(CompressFileWorker);
            workerDelegate.BeginInvoke(asyncOp, null, null);
        }

        /// <summary>
        /// Asynchronous LZF File Decompressor
        /// </summary>
        /// <param name="task"></param>
        public virtual void DecompressFileAsync(object task)
        {
            AsyncOperation asyncOp = AsyncOperationManager.CreateOperation(task);
            lock (userStateToLifetime.SyncRoot)
            {
                if (userStateToLifetime.Contains(task))
                {
                    throw new ArgumentException("Task ID parameter must be unique", "task");
                }
                LZFaTask _task = task as LZFaTask;
                if (_task == null) { throw new InvalidCastException("Task must be typeof(LZFaTask)"); }
                userStateToLifetime[task] = asyncOp;
            }
            LZFaDecompressWorkerEventHandler workerDelegate = new LZFaDecompressWorkerEventHandler(DecompressFileWorker);
            workerDelegate.BeginInvoke(asyncOp, null, null);
        }

        /// <summary>
        /// Cancels an asynchronous worker thread based on the task passed in.
        /// </summary>
        /// <param name="task">
        /// The task must be a typeof(LZFaTask).  If the task has already been canceled or completed, this does nothing.
        /// </param>
        public void CancelAsync(object task)
        {
            AsyncOperation asyncOp = userStateToLifetime[task] as AsyncOperation;
            if (asyncOp != null)
            {
                lock (userStateToLifetime.SyncRoot)
                {
                    userStateToLifetime.Remove(task);
                }
            }
        }

        /*
         * The following methods should permit multiple operation calls to
         * access the dictionary while processing.  To be 'thread-safe',
         * the table is locked to access.  Other threads will have to wait
         * for the lock to be lifted before updates are applied.
         * 
         * Since the table is not marked as 'static' multiple LZFa instances
         * will have different tables to work with.  Using a static table
         * shared among instances should in theory work, it just has not been tested.
         */
        private void LZFTableAddKey(string taskID)
        {
            lock (userStateToLifetime.SyncRoot)
            {
                LZFTable.Add(taskID, null);
            }
        }

        private void LZFTableRemoveKey(string taskID)
        {
            lock (userStateToLifetime.SyncRoot)
            {
                LZFTable.Remove(taskID);
            }
        }

        private void LZFTableClearKeyData(string taskID)
        {
            lock (userStateToLifetime.SyncRoot)
            {
                LZFTable[taskID] = new uint[HSIZE];
            }
        }

        private uint LZFTableGetKeyData(string taskID, uint slot)
        {
            lock (userStateToLifetime.SyncRoot)
            {
                return LZFTable[taskID][slot];
            }
        }

        private void LZFTableSetKeyData(string taskID, uint slot, uint value)
        {
            lock (userStateToLifetime.SyncRoot)
            {
                LZFTable[taskID][slot] = value;
            }
        }


        /// <summary>
        /// Compresses the data using LibLZF algorithm
        /// </summary>
        /// <returns>The size of the compressed archive in the output buffer</returns>
        private int LZFCompress(byte[] input, ref byte[] output, AsyncOperation asyncOp)
        {
            int inputLength = input.Length;
            int outputLength = output.Length;
            LZFaTask task = asyncOp.UserSuppliedState as LZFaTask;
            LZFTableClearKeyData(task.TaskID); //initialize array
            uint hslot;
            uint iidx = 0;
            uint oidx = 0;
            long reference;

            uint hval = (uint)(((input[iidx]) << 8) | input[iidx + 1]); // FRST(in_data, iidx);
            long off;
            int lit = 0;

            for (;;)
            {
                if (TaskCanceled(asyncOp.UserSuppliedState))
                {
                    return -1; //abort, I just couldn't think of a better way to do this.
                }
                if (iidx < inputLength - 2)
                {
                    hval = (hval << 8) | input[iidx + 2];
                    hslot = ((hval ^ (hval << 5)) >> (int)(((3 * 8 - HLOG)) - hval * 5) & (HSIZE - 1));
                    reference = LZFTableGetKeyData(task.TaskID, hslot);
                    LZFTableSetKeyData(task.TaskID, hslot, iidx);

                    if ((off = iidx - reference - 1) < MAX_OFF
                        && iidx + 4 < inputLength
                        && reference > 0
                        && input[reference + 0] == input[iidx + 0]
                        && input[reference + 1] == input[iidx + 1]
                        && input[reference + 2] == input[iidx + 2])
                    {
                        /* match found at *reference++ */
                        uint len = 2;
                        uint maxlen = (uint)inputLength - iidx - len;
                        maxlen = maxlen > MAX_REF ? MAX_REF : maxlen;

                        if (oidx + lit + 1 + 3 >= outputLength)
                        {
                            return 0;
                        }


                        do
                        {
                            len++;
                            if (TaskCanceled(asyncOp.UserSuppliedState))
                            {
                                return -1;
                            }
                        }
                        while (len < maxlen && input[reference + len] == input[iidx + len]);

                        if (lit != 0)
                        {
                            output[oidx++] = (byte)(lit - 1);
                            lit = -lit;
                            do
                            {
                                output[oidx++] = input[iidx + lit];
                                if (TaskCanceled(asyncOp.UserSuppliedState))
                                {
                                    return -1;
                                }
                            }
                            while ((++lit) != 0);
                        }

                        len -= 2;
                        iidx++;

                        if (len < 7)
                        {
                            output[oidx++] = (byte)((off >> 8) + (len << 5));
                        }
                        else
                        {
                            output[oidx++] = (byte)((off >> 8) + (7 << 5));
                            output[oidx++] = (byte)(len - 7);
                        }

                        output[oidx++] = (byte)off;

                        iidx += len - 1;
                        hval = (uint)(((input[iidx]) << 8) | input[iidx + 1]);

                        hval = (hval << 8) | input[iidx + 2];
                        LZFTableSetKeyData(task.TaskID, ((hval ^ (hval << 5)) >> (int)(((3 * 8 - HLOG)) - hval * 5) & (HSIZE - 1)), iidx);
                        iidx++;

                        hval = (hval << 8) | input[iidx + 2];
                        LZFTableSetKeyData(task.TaskID, ((hval ^ (hval << 5)) >> (int)(((3 * 8 - HLOG)) - hval * 5) & (HSIZE - 1)), iidx);
                        iidx++;
                        continue;
                    }
                }
                else
                {
                    if (iidx == inputLength)
                    {
                        break;
                    }
                }

                /* one more literal byte we must copy */
                lit++;
                iidx++;

                if (lit == MAX_LIT)
                {
                    if (oidx + 1 + MAX_LIT >= outputLength)
                    {
                        return 0;
                    }

                    output[oidx++] = (byte)(MAX_LIT - 1);
                    lit = -lit;
                    do
                    {
                        output[oidx++] = input[iidx + lit];
                        if (TaskCanceled(asyncOp.UserSuppliedState))
                        {
                            return -1;
                        }
                    }
                    while ((++lit) != 0);
                }
            }

            if (lit != 0)
            {
                if (oidx + lit + 1 >= outputLength)
                {
                    return 0;
                }

                output[oidx++] = (byte)(lit - 1);
                lit = -lit;
                do
                {
                    output[oidx++] = input[iidx + lit];
                    if (TaskCanceled(asyncOp.UserSuppliedState))
                    {
                        return -1;
                    }
                }
                while ((++lit) != 0);
            }
            return (int)oidx;
        }

        /// <summary>
        /// Decompresses the data using LibLZF algorithm
        /// </summary>
        /// <returns>Returns decompressed size</returns>
        private int LZFDecompress(byte[] input, ref byte[] output, AsyncOperation asyncOp)
        {
            int inputLength = input.Length;
            int outputLength = output.Length;

            uint iidx = 0;
            uint oidx = 0;

            do
            {
                if (TaskCanceled(asyncOp.UserSuppliedState))
                {
                    return -1;
                }

                uint ctrl = input[iidx++];

                if (ctrl < (1 << 5)) /* literal run */
                {
                    ctrl++;

                    if (oidx + ctrl > outputLength)
                    {
                        //SET_ERRNO (E2BIG);
                        return 0;
                    }

                    do
                    {
                        output[oidx++] = input[iidx++];
                        if (TaskCanceled(asyncOp.UserSuppliedState))
                        {
                            return -1;
                        }
                    }
                    while ((--ctrl) != 0);
                }
                else /* back reference */
                {
                    uint len = ctrl >> 5;

                    int reference = (int)(oidx - ((ctrl & 0x1f) << 8) - 1);

                    if (len == 7)
                    {
                        len += input[iidx++];
                    }

                    reference -= input[iidx++];

                    if (oidx + len + 2 > outputLength)
                    {
                        //SET_ERRNO (E2BIG);
                        return 0;
                    }

                    if (reference < 0)
                    {
                        //SET_ERRNO (EINVAL);
                        return 0;
                    }

                    output[oidx++] = output[reference++];
                    output[oidx++] = output[reference++];

                    do
                    {
                        output[oidx++] = output[reference++];
                        if (TaskCanceled(asyncOp.UserSuppliedState))
                        {
                            return -1;
                        }
                    }
                    while ((--len) != 0);
                }
            }
            while (iidx < inputLength);

            return (int)oidx;
        }
    }
}
