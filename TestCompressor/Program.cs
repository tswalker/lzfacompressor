﻿using System;
using System.IO;
using LZFaCompressor;

namespace TestCompressor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press any key to begin operations test..."); //check memory usage in task manager
            Console.ReadLine();
            LZFa _lz;

            //setup for decompression
            var originalFile = Path.Combine("D:\\Temp", "49e77efe-ddc4-400f-adee-f593994cbafe.dds");
            FileInfo _info = new FileInfo(originalFile);
            int initialBufferSize;
            checked { initialBufferSize = (int)_info.Length; }

            //OPERATIONS
            LZFaTask _task1 = new LZFaTask(LZFaTaskType.LZF_COMPRESSION)
            {
                SourceFile = Path.Combine("D:\\Temp", "7d36f796-4d32-4b08-adcb-7c303804bb59.dds"), //7d36f796-4d32-4b08-adcb-7c303804bb59
                TargetFile = Path.Combine("D:\\Temp", "7d36f796.dat"),
                ShimSize = 8192 //a large shim is not required for compressions
            };

            LZFaTask _task2 = new LZFaTask(LZFaTaskType.LZF_DECOMPRESSION)
            {
                SourceFile = Path.Combine("D:\\Temp", "49e77efe.dat"), //49e77efe-ddc4-400f-adee-f593994cbafe
                TargetFile = Path.Combine("D:\\Temp", "49e77efe_result.dds"),
                BufferSize = initialBufferSize, //set to known decompressed file size :)
                ShimSize = 8192 //a shim that is too small is bad for performance if the buffersize is not large enough
            };

            using (_lz = new LZFa())
            {
                _lz.ProgressChanged += LZFaProgressChanged;
                _lz.LZFaCompleted += LZFaCompleted;
                _lz.CompressFileAsync(_task1);
                _lz.DecompressFileAsync(_task2);
            }

            Console.WriteLine("\nPress any key to continue (or cancel).");
            while (TaskActive(_task1.TaskState)) //this acts like a coroutine typeof(terrible)
            {
                Console.ReadLine();
                if (_lz != null)
                {
                    _lz.CancelAsync(_task1);
                    _lz.CancelAsync(_task2);
                }
                break;
            }

            //tasks are update in the thread and handed back, accessible in both main thread, worker thread, and passed to event handler in e.
            Console.WriteLine("Task 1 (" + _task1.TaskID + "),   Compression: " + _task1.CompressedSize);
            Console.WriteLine("Task 2 (" + _task2.TaskID + "), Decompression: " + _task2.DecompressedSize);
            _lz.LZFaCompleted -= LZFaCompleted;
            _lz.ProgressChanged -= LZFaProgressChanged;
            if (_lz != null)
            {
                _lz = null;
            }
            GC.Collect(); //check memory use for TestCompressor.. should drop down to atleast (or below) start size
            Console.WriteLine("Press any key to close.");
            Console.ReadLine();
        }

        private static bool TaskActive(LZFaTaskState state)
        {
            switch (state)
            {
                case LZFaTaskState.CANCELED:
                    return false;
                case LZFaTaskState.COMPLETED:
                    return false;
                case LZFaTaskState.ERROR:
                    return false;
            }
            return true;
        }

        private static void LZFaProgressChanged(object sender, LZFaProgressChangedEventArgs e)
        {
            if (e != null)
            {
                LZFaTask _task = e.UserState as LZFaTask;
                string _taskID = string.Empty;
                if (_task != null)
                {
                    _taskID = _task.TaskID;
                }
                Console.WriteLine("Task State: " + e.TaskState + "; TaskID: " + _taskID);
            }
        }

        private static void LZFaCompleted(object sender, LZFaCompletedEventArgs e)
        {
            if (!e.Cancelled)
            {
                try
                {
                    LZFaTask task = e.Task;
                    if (task != null)
                    {
                        var _compB = task.CompressedSize;
                        var _uncpB = task.DecompressedSize;
                        var _ratio = (float)_compB * 100 / _uncpB;
                        var _result = 100f - _ratio;
                        Console.WriteLine("Task: " + task.TaskID + "; Compressed: " + _compB.ToString() + "; Uncompressed: " + _uncpB.ToString() + "; ratio: " + _result.ToString("0.00") + "%");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("(Task) Exception Occurred: " + ex.InnerException);
                }
            }
            else
            {
                try
                {
                    LZFaTask task = e.Task;
                    string error = string.Empty;
                    if (e != null && e.Error != null)
                    {
                        error = "; [ERROR]: " + e.Error.Message;
                    }
                    if (task != null)
                    {
                        Console.WriteLine("Task: " + task.TaskID + "; Cancelled " + error);
                    }
                    else
                    {
                        Console.WriteLine("(Task) Propagation error.");
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
