LZFaCompressor is an implementation of the LZF algorithm for use in Unity 4.x and 5.x (and most likely with 201x).

The library is compatible with .NET 3.x source for component based asynchronous processing using the EAP Pattern and tasks.

An example executable is provided to show how the source may be used.

This solution is currently used in production systems with zero code base changes in iOS and UWP.
